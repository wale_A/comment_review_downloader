﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using UtilityClass;

namespace CommentAndReviewDownloader.Controllers
{
    public class FilesController : Controller
    {
        public FileResult Download(string xyz)
        {
            using (var dbContext = DatabaseContext.Create())
            {
                var request = dbContext.Requests.FirstOrDefault(x => x.ID == xyz);
                if (request != null && request.File != null)
                {
                    var stream = new MemoryStream(request.File);
                    return new FileStreamResult(stream, "text/csv") { FileDownloadName = DateTime.Now.Ticks.ToString() + ".csv" };
                }

                return null;
            }
        }

    }
}
