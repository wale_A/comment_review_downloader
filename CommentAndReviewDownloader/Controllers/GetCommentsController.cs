﻿using CommentAndReviewDownloader.Misc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CommentAndReviewDownloader.Controllers
{
    public class VideoCommentsController : Controller
    {
        const string YOUTUBEKEY = "AIzaSyBt0LME9fta_z6L3vSG2RLeG5aL7j97UTU";

        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> GetVideoComment(string pageUrl)
        {
            var uri = new Uri(pageUrl);
            string host = uri.Host.ToLower();

            if (host.Contains("youtube"))
            {
                var query = uri.Query;
                var videoId = query.Split(new char[] { '=' })[1];

                //get number of comments
                var httpClient = new HttpClient();
                var responseMessage = await httpClient.GetAsync($"https://www.googleapis.com/youtube/v3/videos?id={videoId}&key={YOUTUBEKEY}&part=statistics");
                var responseString = await responseMessage.Content.ReadAsStringAsync();

                var responseObject = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);
                var videoResponseBody = responseObject.items;

                if (videoResponseBody == null)
                { }
                else
                {
                    var totalCommentCount = videoResponseBody[0].statistics.commentCount;
                    outputArray = new Models.Output[totalCommentCount];

                    string nextPageToken = string.Empty;
                    for (int i = 0; i < totalCommentCount;)
                    {
                        var _httpClient = new HttpClient();
                        var _responseMessage = await httpClient.GetAsync($"https://www.googleapis.com/youtube/v3/commentThreads?key={YOUTUBEKEY}&textFormat=plainText&part=snippet&videoId={videoId}&maxResults=100&pageToken={nextPageToken}");
                        var _responseString = await responseMessage.Content.ReadAsStringAsync();
                        var _responseObject = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(responseString);

                        nextPageToken = _responseObject.nextPageToken;
                        var commentList = _responseObject.items;
                        int pageCommentCount = _responseObject.pageInfo.totalResults;

                        for (int x = 0; x < pageCommentCount; x++, i++)
                        {
                            var commentObject = commentList[x].snippet.topLevelComment.snippet;
                            string comment = commentObject.textDisplay;
                            string username = commentObject.authorDisplayName;
                            string link = commentObject.authorProfileImageUrl;
                            string date = commentObject.updatedAt;

                            outputArray[i] = new Models.Output(username, date, "", comment, link);
                        }
                    }

                }
            }

            return View();
        }
    }
}