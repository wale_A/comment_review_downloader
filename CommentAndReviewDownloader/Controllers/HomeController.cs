﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UtilityClass;

namespace CommentAndReviewDownloader.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SubmitRequest( string pageUrl, string userEmail)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pageUrl)) { throw new Exception("please enter a valid url"); }
                if (string.IsNullOrWhiteSpace(userEmail)) { throw new Exception("please enter a valid email"); }

                var userRequest = new UserRequest() { RequestDateTime = DateTime.Now, ReplyDateTime = DateTime.MinValue, Status = RequestStatus.Pending, UserEmail = userEmail, RequestUrl = pageUrl, CancellationReason = "", ID = Guid.NewGuid().ToString("N") };

                var dbContext = DatabaseContext.Create();
                dbContext.Requests.Add(userRequest);
                dbContext.SaveChanges();

                return View("Success");
            } catch(Exception ex)
            {
                TempData["ErrorMessage"] = ex.Message;
                return View("Index");
            }
        }

        
    }
}