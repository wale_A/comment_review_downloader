﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UtilityClass;

namespace CommentAndReviewWindowsService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            Thread thread = new Thread(new ThreadStart(checkPendingRequests));
            thread.Start();
        }

        private void checkPendingRequests()
        {
            try
            {
                while (true)
                {
                    using (var dbcontext = DatabaseContext.Create())
                    {
                        var requests = dbcontext.Requests.Where(x => x.Status == RequestStatus.Pending).ToList();
                        foreach (var req in requests)
                        {
                            Task.Run(() => HandlePendingRequest(req));
                        }
                    }

                    Thread.Sleep(2 * 60 * 1000);
                }
            }
            catch (Exception ex) { }
        }

        private void HandlePendingRequest(UserRequest userRequest)
        {
            userRequest.Status = RequestStatus.InProgress;
            changeRequestStatus(userRequest);

            string fileName = string.Empty;

            try
            {
                ReviewDownloader rd;
                var uri = new Uri(userRequest.RequestUrl);
                string host = uri.Host.ToLower();

                if (host.Contains("amazon"))
                {
                    rd = new AmazonReviewDownloader();
                    fileName = rd.PerformOp(uri);
                }
                else if (host.Contains("youtube"))
                {
                    rd = new YoutubeReviewDownloader();
                    fileName = rd.PerformOp(uri);
                }
                else
                {
                    throw new Exception($"Url - {userRequest.RequestUrl} is invalid");
                }

                userRequest.File = System.IO.File.ReadAllBytes(fileName);
                userRequest.ReplyDateTime = DateTime.Now;
                userRequest.Status = RequestStatus.Completed;
            }
            catch (Exception ex)
            {
                userRequest.Status = RequestStatus.Cancelled;
                userRequest.CancellationReason = ex.Message;
            }

            changeRequestStatus(userRequest);
            sendMail(userRequest);
        }

        private static void changeRequestStatus(UserRequest userRequest)
        {
            using (var dbcontext = DatabaseContext.Create())
            {
                dbcontext.Entry(userRequest).State = System.Data.Entity.EntityState.Modified;
                dbcontext.SaveChanges();
            }
        }

        private void sendMail(UserRequest userRequest)
        {
            try
            {
                SmtpClient client = new SmtpClient("hellocare.ng", 25);
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress("wale@hellocare.ng");
                mail.To.Add(userRequest.UserEmail);
                mail.Subject = "Comment/Review Downloader";
                mail.IsBodyHtml = true;

                if (userRequest.Status == RequestStatus.Cancelled)
                {
                    mail.Body = $"unable to get comments/review <br><br>" +
                        $"{userRequest.CancellationReason} <br><br>" +
                        $"please retry ";
                }
                else if (userRequest.Status == RequestStatus.Completed)
                {
                    mail.Body = $"successfully curated your comments/review for  {userRequest.RequestUrl} <br><br>" +
                        $"<a href=\"http://thegiveaway.xyz/files/download?xyz={userRequest.ID}\">" +
                        $"download file</a> <br><br>" +
                        $"THANKS ";
                }

                client.Credentials = new NetworkCredential("wale@hellocare.ng", "password");
                client.Send(mail);
            }
            catch (Exception ex) {
                string s = "";
            }
        }

        internal void StartRunning()
        {
            OnStart(null);
        }

        protected override void OnStop()
        {
        }
    }
}
