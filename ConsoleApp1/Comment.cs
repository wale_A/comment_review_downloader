﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace ConsoleApp1
{
    public class Output
    {
        public string Username { get; set; }
        public string Date { get; set; }
        public string StarRating { get; set; }
        public string Comment { get; set; }
        public string Link { get; set; }

        public Output(string username, string date, string starRating, string comment, string link)
        {
            
            Username = Regex.Replace(username, @"\t|\r?\n|\r/g", ""); ;
            Date = date;
            StarRating = starRating;
            Comment = Regex.Replace(comment, @"\t|\r?\n|\r/g", ""); ;
            Link = link;
        }
    }
}