﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static List<Output> outputArray;
        const string YOUTUBEKEY = "AIzaSyBEwIzhP2VAfG53V--ApO2AQL958oWSn8o";

        static void Main(string[] args)
        {
            Console.WriteLine("Enter url");
            var url = Console.ReadLine();

            GetYoutubeComments(url).Wait();


        }


        private static async Task GetYoutubeComments(string pageUrl)
        {
            Console.WriteLine($"{DateTime.Now.ToLongTimeString()} {Environment.NewLine}");

            var uri = new Uri(pageUrl);
            string host = uri.Host.ToLower();

            if (host.Contains("youtube"))
            {
                var query = uri.Query;
                var videoId = query.Split(new char[] { '=' })[1];

                outputArray = new List<Output>();
                string nextPageToken = string.Empty;
                do
                {
                    var _httpClient = new HttpClient();
                    var _responseMessage = await _httpClient.GetAsync($"https://www.googleapis.com/youtube/v3/commentThreads?key={YOUTUBEKEY}&textFormat=plainText&part=snippet&videoId={videoId}&maxResults=100&pageToken={nextPageToken}");

                    if (_responseMessage.IsSuccessStatusCode)
                    {
                        var _responseString = await _responseMessage.Content.ReadAsStringAsync();
                        var _responseObject = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(_responseString);

                        nextPageToken = _responseObject.nextPageToken ?? "";
                        var commentList = _responseObject.items;
                        int pageCommentCount = _responseObject.pageInfo.totalResults;

                        for (int x = 0; x < pageCommentCount; x++)
                        {
                            var commentObject = commentList[x].snippet.topLevelComment.snippet;
                            string commentId = commentList[x].snippet.topLevelComment.id;
                            int commentReplyCount = commentList[x].snippet.totalReplyCount ?? 0;
                            string comment = commentObject.textDisplay;
                            string username = commentObject.authorDisplayName;
                            string link = commentObject.authorProfileImageUrl;
                            string date = commentObject.updatedAt;

                            outputArray.Add(new Output(username, date, " ", comment, link));
                            //try {  }
                            //catch { }

                            string _nextPageToken = string.Empty;
                            if (commentReplyCount > 0)
                            {
                                do
                                {
                                    //_httpClient = new HttpClient();
                                    _responseMessage = await _httpClient.GetAsync($"https://www.googleapis.com/youtube/v3/comments?key=AIzaSyBt0LME9fta_z6L3vSG2RLeG5aL7j97UTU&textFormat=plainText&part=snippet&parentId={commentId}&maxResults=100&pageToken={_nextPageToken}");

                                    if (_responseMessage.IsSuccessStatusCode)
                                    {
                                        _responseString = await _responseMessage.Content.ReadAsStringAsync();
                                        _responseObject = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(_responseString);
                                        _nextPageToken = _responseObject.nextPageToken ?? "";
                                        var _commentList = _responseObject.items;

                                        foreach (var _innerComment in _commentList)
                                        {
                                            commentObject = _innerComment.snippet;
                                            comment = commentObject.textDisplay;
                                            commentId = _innerComment.id;
                                            username = commentObject.authorDisplayName;
                                            link = commentObject.authorProfileImageUrl;
                                            date = commentObject.updatedAt;

                                            outputArray.Add(new Output(username, date, " ", comment, link));
                                            //try {  }
                                            //catch { }
                                        }
                                    }
                                } while (!string.IsNullOrWhiteSpace(_nextPageToken));
                            }
                        }
                    }
                } while (!string.IsNullOrEmpty(nextPageToken));

                var fileName = Guid.NewGuid().ToString("N") + ".csv";
                using (StreamWriter writer = new StreamWriter(fileName))
                {
                    writer.WriteLine("Username, Date, StarRating, Comment/Review, Link");
                    foreach (var comment in outputArray)
                    {
                        writer.WriteLine($"{comment.Username}, {comment.Date}, {comment.StarRating}, {comment.Comment}, {comment.Link}");
                    }
                }

                Console.WriteLine($"{DateTime.Now.ToLongTimeString()} {Environment.NewLine}");

                Console.ReadKey();
            }
        }
    }
}
