﻿using HtmlAgilityPack;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClass
{
    public class AmazonReviewDownloader : ReviewDownloader
    {
        protected override void saveReviews()
        {
            if (requestUri.Segments.Count() < 3) { throw new Exception(""); }
            var productId = requestUri.Segments[3];

            string nextPageLink = null;

            do
            {
                var link = nextPageLink ?? $"{requestUri.Scheme}://{requestUri.Host}/product-reviews/{productId}";
                var _client = new RestClient();
                _client.CookieContainer = new System.Net.CookieContainer();
                _client.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                _client.BaseUrl = new Uri(link);

                var request = new RestRequest();
                var _restResponse = _client.Get(request);

                var _responseString = _restResponse.Content;
                var htmlDoc = new HtmlAgilityPack.HtmlDocument();
                htmlDoc.LoadHtml(_responseString);

                nextPageLink = getNextPageLink(htmlDoc);

                // div   class="a-section celwidget"
                var reviewDivs = htmlDoc.DocumentNode.Descendants("div").Where(x => x.HasClass("a-section") && x.HasClass("celwidget") && x.FirstChild.HasClass("a-row") && x.FirstChild.Name == "div").ToList();

                string comment = "";
                string date = "";
                string name = "";
                string userlink = "";
                string rating = "";

                for (var i = 0; i < reviewDivs.Count; i++)
                {
                    var neededNodes = reviewDivs.ElementAt(i).SelectNodes("//*[@data-hook]").ToList();

                    comment = neededNodes.Where(x => x.Attributes["data-hook"].Value == "review-body" && x.Name == "span").ElementAtOrDefault(i).InnerText;
                    date = neededNodes.Where(x => x.Attributes["data-hook"].Value == "review-date" && x.Name == "span").ElementAtOrDefault(i).InnerText.Replace("on ", "");
                    rating = neededNodes.Where(x => x.Attributes["data-hook"].Value == "review-star-rating" && x.Name == "i").ElementAtOrDefault(i).FirstChild.InnerHtml.Replace(" out of 5 stars", "");
                    name = neededNodes.Where(x => x.Attributes["data-hook"].Value == "review-author" && x.Name == "a").ElementAtOrDefault(i).InnerText;
                    userlink = requestUri.Host + neededNodes.Where(x => x.Attributes["data-hook"].Value == "review-author" && x.Name == "a").ElementAtOrDefault(i).Attributes["href"].Value;

                    outputArray.Add(new Output(name, date, rating, comment, userlink));
                }
            } while (!string.IsNullOrWhiteSpace(nextPageLink));

        }

        private string getNextPageLink(HtmlDocument htmlDoc)
        {
            var linkNodes = htmlDoc.DocumentNode.SelectNodes("//link[@rel]").ToList();

            for (int i = 0; i < linkNodes.Count; i++)
            {
                var relAttribute = linkNodes[i].Attributes["rel"].Value;
                if (relAttribute == "next")
                {
                    return linkNodes[i].Attributes["href"].Value;
                }
            }
            return string.Empty;
        }
    }
}
