﻿using MySql.Data.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace UtilityClass
{
    [DbConfigurationType(typeof(MySqlEFConfiguration))]
    public class DatabaseContext: DbContext
    {
        public DatabaseContext(): base("connectionString")
        { }

        public static DatabaseContext Create()
        {
            return new DatabaseContext();
        }

        public DbSet<UserRequest> Requests { get; set; }

    }
}