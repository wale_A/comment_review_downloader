﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClass
{
    public abstract class ReviewDownloader
    {
        public Uri requestUri { get; set; }
        public List<Output> outputArray;

        public string PerformOp(Uri uri)
        {
            requestUri = uri;
            outputArray = new List<Output>();
            saveReviews();
            return writeToFile();
        }

        protected abstract void saveReviews();

        private string writeToFile()
        {
            var fileName = Guid.NewGuid().ToString("N") + ".csv";
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.WriteLine("Username, Date, StarRating, Comment/Review, Link");
                foreach (var comment in outputArray)
                {
                    writer.WriteLine($"{comment.Username}, {comment.Date}, {comment.StarRating}, {comment.Comment}, {comment.Link}");
                }
            }
            return fileName;
        }
    }
}
