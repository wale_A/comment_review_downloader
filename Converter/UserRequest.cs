﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace UtilityClass
{
    public class UserRequest
    {
        public string ID { get; set; }
        public string RequestUrl { get; set; }
        public DateTime RequestDateTime { get; set; }
        public string UserEmail { get; set; }
        public DateTime ReplyDateTime { get; set; }
        public RequestStatus Status { get; set; }
        public string CancellationReason { get; set; }
        public byte[] File { get; set; }
    }
    
    public enum RequestStatus
    {
        Pending,
        InProgress,
        Completed,
        Cancelled
    }       

}