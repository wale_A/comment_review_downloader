﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UtilityClass
{
    public class YoutubeReviewDownloader : ReviewDownloader
    {
        const string YOUTUBEKEY = "AIzaSyBEwIzhP2VAfG53V--ApO2AQL958oWSn8o";

        protected override void saveReviews()
        {
            var query = requestUri.Query;
            var videoId = query.Split(new char[] { '=' })[1];

            outputArray = new List<Output>();
            string nextPageToken = string.Empty;
            do
            {
                var _restClient = new RestClient();
                _restClient.BaseUrl = new Uri("https://www.googleapis.com/youtube/v3");
                var request = new RestRequest($"/commentThreads?key={YOUTUBEKEY}&textFormat=plainText&part=snippet&videoId={videoId}&maxResults=100&pageToken={nextPageToken}");
                var _responseString = _restClient.Get(request).Content;
                var _responseObject = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(_responseString);

                nextPageToken = _responseObject.nextPageToken ?? "";
                var commentList = _responseObject.items;
                int pageCommentCount = _responseObject.pageInfo.totalResults;

                for (int x = 0; x < pageCommentCount; x++)
                {
                    var commentObject = commentList[x].snippet.topLevelComment.snippet;
                    string commentId = commentList[x].snippet.topLevelComment.id;
                    int commentReplyCount = commentList[x].snippet.totalReplyCount ?? 0;
                    string comment = commentObject.textDisplay;
                    string username = commentObject.authorDisplayName;
                    string link = commentObject.authorProfileImageUrl;
                    string date = commentObject.updatedAt;
                    outputArray.Add(new Output(username, date, " ", comment, link));

                    string _nextPageToken = string.Empty;
                    if (commentReplyCount > 0)
                    {
                        do
                        {
                            request = new RestRequest($"/comments?key={YOUTUBEKEY}&textFormat=plainText&part=snippet&parentId={commentId}&maxResults=100&pageToken={_nextPageToken}");
                            _responseString = _restClient.Get(request).Content;
                            _responseObject = (dynamic)Newtonsoft.Json.JsonConvert.DeserializeObject(_responseString);

                            _nextPageToken = _responseObject.nextPageToken ?? "";
                            var _commentList = _responseObject.items;

                            foreach (var _innerComment in _commentList)
                            {
                                comment = _innerComment.snippet.textDisplay;
                                username = _innerComment.snippet.authorDisplayName;
                                link = _innerComment.snippet.authorProfileImageUrl;
                                date = _innerComment.snippet.updatedAt;

                                outputArray.Add(new Output(username, date, " ", comment, link));
                            }
                        } while (!string.IsNullOrWhiteSpace(_nextPageToken));
                    }
                }
            } while (!string.IsNullOrEmpty(nextPageToken));
        }
    }
}
